using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class RealFramerate : MonoBehaviour {

    public int framerate { get { return _framerate; } }
    private int _framerate;

    public bool maxPower = true;
    private bool _maxPower = true;

    private Queue<float> _deltas = new Queue<float>();

    private GUIStyle _style;
    private GUIStyle style {
        get {
            if (_style == null) {
                _style = new GUIStyle(GUI.skin.label);
                _style.fontSize = 50;
                _style.normal.textColor = ThemeRefs.Accent;
            }
            return _style;
        }
    }

    private void Awake() {
        Application.targetFrameRate = -1;
        QualitySettings.vSyncCount = 0;
    }

    void Update () {
        if (_maxPower != maxPower) {
            _maxPower = maxPower;
            Application.targetFrameRate = _maxPower? -1 : 60;
            QualitySettings.vSyncCount = _maxPower? 0 : 1;
        }
        _deltas.Enqueue(Time.deltaTime);
        _framerate = (int)Mathf.Round(1f / _deltas.Sum(o => o) * _deltas.Count);
        int dampFrames = Mathf.Clamp(_framerate, 1, 250);

        while (_deltas.Count > dampFrames)
            _deltas.Dequeue();
    }

    private void OnGUI() {
        GUI.Label(new Rect(50, 50, 400, 150), _framerate + " FPS", style);
    }
}