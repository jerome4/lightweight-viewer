﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public enum MaterialType
{
    Default,
    ClipPlane
}

public class MaterialData
{
    public Dictionary<MaterialType, List<MaterialDataVariant>> variants;
    public Renderer renderer;

    public MaterialData()
    {
        variants = new Dictionary<MaterialType, List<MaterialDataVariant>>();
    }

    public MaterialData Clone()
    {
        MaterialData clone = new MaterialData();

        clone.variants = new Dictionary<MaterialType, List<MaterialDataVariant>>();

        foreach(KeyValuePair<MaterialType, List<MaterialDataVariant>> pair in variants)
        {
            clone.variants.Add(pair.Key, null);
            clone.variants[pair.Key] = new List<MaterialDataVariant>();

            for(int i =0; i < pair.Value.Count; ++i)
            {
                clone.variants[pair.Key][i] = pair.Value[i].Clone();
            }
        }

        clone.renderer = renderer;

        return clone;
    }
}

public class MaterialDataVariant
{
    public Material material;
    public MaterialType matType;

    public MaterialDataVariant()
    {
    }

    public MaterialDataVariant(Material mat, MaterialType type)
    {
        material = mat;
        matType = type;
    }

    public MaterialDataVariant Clone()
    {
        MaterialDataVariant clone = new MaterialDataVariant();

        clone.material = material;
        clone.matType = matType;

        return clone;
    }
}

public class MaterialBank
{
    private List<MaterialData> _materialsData;

    private Material normalsMaterial;
    private Material verticesMaterial;
    private Material densityMaterial;
    private Material vertexColorMaterial;
    private Material wireframeMaterial;
    private Material uvsMaterial;

    public Material UVsMaterial
    {
        get
        {
            if (uvsMaterial == null)
            {
                uvsMaterial = new Material(Shader.Find("PiXYZ/UV Overlay"));
                uvsMaterial.SetTexture(Shader.PropertyToID("_UVTex"), ImageRefs.UvTexture);
            }
            return uvsMaterial;
        }
    }
    public Material NormalsMaterial => normalsMaterial ?? (normalsMaterial = new Material(Shader.Find("PiXYZ/Normals Overlay")));
    public Material WireframeMaterial => wireframeMaterial ?? (wireframeMaterial = new Material(Shader.Find("PiXYZ/Wireframe Overlay")));
    public Material VertexColorMaterial => vertexColorMaterial ?? (vertexColorMaterial = new Material(Shader.Find("PiXYZ/Vertex Color Overlay")));
    public Material DensityMaterial => densityMaterial ?? (densityMaterial = new Material(Shader.Find("PiXYZ/Density Overlay")));
    public Material VerticesMaterial => verticesMaterial ?? (verticesMaterial = new Material(Shader.Find("PiXYZ/Vertex Overlay")));

    public ReadOnlyCollection<MaterialData> MaterialsData { get { return _materialsData.AsReadOnly(); } }
    
    public MaterialBank()
    {
        _materialsData = new List<MaterialData>();
    }

    public void Initialize(Transform root)
    {
        _materialsData.Clear();

        Renderer[] renderers = root.GetComponentsInChildren<Renderer>();
        Dictionary<Material, List<MaterialDataVariant>> variantsTmp = new Dictionary<Material, List<MaterialDataVariant>>();

        Debug.Log("[Material Bank] Initializing...");
        foreach(Renderer renderer in renderers)
        {
            MaterialData data = new MaterialData();

            foreach (Material mat in renderer.sharedMaterials)
            {
                MaterialDataVariant variant = null;
                List<MaterialDataVariant> variantList = null;

                if (variantsTmp.ContainsKey(mat))
                {
                    variantList = variantsTmp[mat];
                }
                else
                {
                    variantList = new List<MaterialDataVariant>();
                    variantsTmp.Add(mat, variantList);
                }

                if (GenerateClipPlaneVariant(mat, data, out variant, variantsTmp[mat]))
                {
                    variantsTmp[mat].Add(variant);
                }

                variantsTmp[mat].Add(GenerateDefaultMaterialVariant(mat, data));
            }

            data.renderer = renderer;

            _materialsData.Add(data);
        }

        Debug.Log("[Material Bank] Initialization finished !(mats : "+variantsTmp.Keys.Count+" | Renderers : " + renderers.Length+")");
    }

    private MaterialDataVariant GenerateDefaultMaterialVariant(Material material, MaterialData data)
    {
        if (!data.variants.ContainsKey(MaterialType.Default))
            data.variants.Add(MaterialType.Default, new List<MaterialDataVariant>());

        MaterialDataVariant variant = new MaterialDataVariant(material, MaterialType.Default);
        data.variants[MaterialType.Default].Add(variant);

        return variant;
    }

    private bool GenerateClipPlaneVariant(Material material, MaterialData data, out MaterialDataVariant variant, List<MaterialDataVariant> source = null)
    {
        if (source != null)
        {
            variant = source.Find(m => m.matType == MaterialType.ClipPlane);
            if(variant != null)
            {
                variant = variant.Clone();

                if (!data.variants.ContainsKey(MaterialType.ClipPlane))
                    data.variants.Add(MaterialType.ClipPlane, new List<MaterialDataVariant>());

                data.variants[MaterialType.ClipPlane].Add(variant);

                return false;
            }
        }

        variant = new MaterialDataVariant();
        variant.matType = MaterialType.ClipPlane;

        Material clipPlaneMaterial = new Material(Shader.Find("PiXYZ/ClipPlane"));

        clipPlaneMaterial.SetVector("_Color", material.GetVector("_Color"));
        clipPlaneMaterial.SetFloat("_Metallic", material.GetFloat("_Metallic"));
        clipPlaneMaterial.SetFloat("Smoothness", material.GetFloat("_Glossiness"));
        clipPlaneMaterial.SetVector("_Emission", material.GetVector("_EmissionColor"));

        clipPlaneMaterial.SetTexture("_MainTex", material.GetTexture("_MainTex"));
        clipPlaneMaterial.SetTexture("_MetallicTex", material.GetTexture("_MetallicGlossMap"));
        clipPlaneMaterial.SetTexture("_BumpMap", material.GetTexture("_BumpMap"));

        variant.material = clipPlaneMaterial;

        if (!data.variants.ContainsKey(MaterialType.ClipPlane))
            data.variants.Add(MaterialType.ClipPlane, new List<MaterialDataVariant>());

        data.variants[MaterialType.ClipPlane].Add(variant);

        return true;
    }
}
