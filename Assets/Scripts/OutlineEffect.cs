using UnityEngine;
using UnityEngine.Rendering;

namespace PiXYZ
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Camera))]
    public class OutlineEffect : MonoBehaviour
    {
        public Material outlineMaterial;
        public Material bufferMaterial;

        private Camera sourceCamera;
        private RenderTexture renderTexture;
        private Color previousColor;
        private CommandBuffer commandBuffer;

        void Start()
        {
            outlineMaterial.SetColor("_LineColor", ThemeRefs.Accent);

            sourceCamera = GetComponent<Camera>();

            Selection.changed.AddListener(SelectedChanged);

            renderTexture = new RenderTexture(sourceCamera.pixelWidth, sourceCamera.pixelHeight, 16, RenderTextureFormat.Default);

            commandBuffer = new CommandBuffer();
            commandBuffer.name = "Outline Effect";
            sourceCamera.AddCommandBuffer(CameraEvent.BeforeImageEffects, commandBuffer);
        }

        private void SelectedChanged()
        {
            RefreshCommandBuffer();
        }

        private void RefreshCommandBuffer()
        {
            if (commandBuffer == null)
                return;

            commandBuffer.Clear();
            commandBuffer.SetRenderTarget(renderTexture);
            commandBuffer.ClearRenderTarget(true, true, Color.clear);
            if (enabled && Selection.Selected != null) {
                Renderer[] renderers = Selection.Selected.GetComponentsInChildren<Renderer>();
                for (int i = 0; i < renderers.Length; i++) {
                    var submeshCount = renderers[i].GetComponent<MeshFilter>()?.mesh?.subMeshCount;
                    for (int j = 0; j < submeshCount; j++)
                    {
                        commandBuffer.DrawRenderer(renderers[i], bufferMaterial, j, 0);
                    }
                }
            }
        }

        public void OnPreRender()
        {
            // Updates render texture if required
            if (renderTexture == null || renderTexture.width != sourceCamera.pixelWidth || renderTexture.height != sourceCamera.pixelHeight) {
                renderTexture = new RenderTexture(sourceCamera.pixelWidth, sourceCamera.pixelHeight, 16, RenderTextureFormat.Default);
            }
        }

        void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            outlineMaterial.SetTexture("_OutlineSource", renderTexture);
            Graphics.Blit(source, destination, outlineMaterial, 1);
        }

        private void OnEnable()
        {
            RefreshCommandBuffer();
        }

        private void OnDisable()
        {
            RefreshCommandBuffer();
        }

        void OnDestroy()
        {
            if (commandBuffer == null)
                return;

            sourceCamera.RemoveCommandBuffer(CameraEvent.BeforeImageEffects, commandBuffer);
            commandBuffer.Clear();
        }
    }
}