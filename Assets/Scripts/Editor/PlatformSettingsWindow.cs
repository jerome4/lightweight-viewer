﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class PlatformSettingsWindow : EditorWindow
{
    [MenuItem("Universal Viewer/Builder Window")]
    private static void OpenBuilder()
    {
        PlatformSettingsWindow builder = CreateInstance<PlatformSettingsWindow>();
        builder.Show();
    }

    private int selectedTab;

    private Editor modelsEditor;
    private Editor ModelsEditor {
        get {
            if (!modelsEditor) {
                modelsEditor = Editor.CreateEditor(PlatformSettings.Instance);
            }
            return modelsEditor;
        }
    }

    private void OnGUI()
    {
        GUILayout.Space(10);
        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        string[] titles = { "Current", "Supported" };
        selectedTab = GUILayout.Toolbar(selectedTab, titles, "LargeButton", GUI.ToolbarButtonSize.FitToContents);
        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();

        GUILayout.Space(10);

        switch (selectedTab) {
            case 0:
                DrawCurrent();
                break;
            case 1:
                DrawSupported();
                break;
        }
    }

    private void DrawCurrent()
    {
        var buildTarget = (BuildTarget)EditorGUILayout.EnumPopup("Target Platform", EditorUserBuildSettings.activeBuildTarget);
        if (buildTarget != EditorUserBuildSettings.activeBuildTarget) {
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildPipeline.GetBuildTargetGroup(buildTarget), buildTarget);
        }

        GUILayout.Space(10);
        EditorGUILayout.LabelField("Interactions", EditorStyles.boldLabel);

        PlatformSetting supportedInteractionsForPlatform = PlatformSettings.Instance.platforms.Where(x => x.buildTarget == buildTarget).FirstOrDefault();

        if (supportedInteractionsForPlatform == null) {
            EditorGUILayout.HelpBox($"Platform '{buildTarget}' is not supported in UniversalViewer.", MessageType.Error);
        } else {
            int i = 0;
            var includedInteractionModels = new List<InteractionModel>();
            foreach (InteractionModel interactionMode in supportedInteractionsForPlatform.interactionModels) {
                if (interactionMode == null) {
                    Debug.LogWarning("Missing InteractionModel !");
                    continue;
                }
                bool previousValue = (i == 0) || InteractionRefs.ReferencedModels.Contains(interactionMode);
                bool newValue = EditorGUILayout.ToggleLeft(interactionMode.name + ((i == 0) ? " (by default)" : null), previousValue);
                newValue = (i == 0) || newValue;
                if (newValue)
                    includedInteractionModels.Add(interactionMode);
                i++;
            }
            bool referencesChanged = false;
            for (int j = 0; j < includedInteractionModels.Count; j++) {
                if (InteractionRefs.ReferencedModels.Count != includedInteractionModels.Count) {
                    referencesChanged = true;
                    break;
                }
                if (InteractionRefs.ReferencedModels[j] != includedInteractionModels[j]) {
                    referencesChanged = true;
                    break;
                }
            }
            if (referencesChanged) {
                InteractionRefs.ReferencedModels = includedInteractionModels;
            }
        }
    }

    private void DrawSupported()
    {
        for (int i = 0; i < PlatformSettings.Instance.platforms.Length; i++) {
            PlatformSettings.Instance.platforms[i].SetName();
        }
        ModelsEditor.OnInspectorGUI();
    }
}
