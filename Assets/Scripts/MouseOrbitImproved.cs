﻿using UnityEngine;
using UnityEngine.EventSystems;

public class MouseOrbitImproved : MonoBehaviour
{
    public Material lineMaterial;
    public Transform target;

    public float distance = 5.0f;
    public float xSpeed = 120.0f;
    public float ySpeed = 120.0f;

    const float Y_MIN = -89.9f;
    const float Y_MAX = 89.9f;

    public float distanceMin = .5f;
    public float distanceMax = 15f;

    private float x = 0.0f;
    private float y = 0.0f;

    private Vector2 startPos;

    private bool isOverUI = false;
    private float zoom;

    private Vector3 touchPosAtStart;

    void Start()
    { 
        Vector3 angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;

        Fit();
        Root.changed.AddListener(Fit);
    }

    public void Fit()
    {
        Bounds? boundsn = target.GetTreeBounds();

        if (boundsn == null) {
            return;
        }

        Bounds bounds = (Bounds)boundsn;
        Vector3 tmp = 0.6f * Vector3.Scale(Vector3.Cross(Vector3.one, this.transform.forward), bounds.size);
        distance = 4f * Mathf.Max(Mathf.Abs(tmp.x), Mathf.Abs(tmp.y), Mathf.Abs(tmp.z));
        distanceMin = (bounds.max - bounds.min).magnitude / 2;
        distanceMax = 10 * distanceMin;
    }
        
    void OnPostRender()
    {
        if (!isOverUI && (Input.GetMouseButton(0) || Input.touches.Length > 0)) {
            GL.PushMatrix();
            GL.Begin(GL.LINES);
            lineMaterial?.SetPass(0);
            GL.Color(Color.red);
            GL.Vertex3(0f, 0f, 0f);
            GL.Vertex3(1f, 0f, 0f);
            GL.Color(Color.green);
            GL.Vertex3(0f, 0f, 0f);
            GL.Vertex3(0f, 1f, 0f);
            GL.Color(Color.blue);
            GL.Vertex3(0f, 0f, 0f);
            GL.Vertex3(0f, 0f, 1f);
            GL.End();
            GL.PopMatrix();
        }
    }
        
    void LateUpdate()
    {

#if UNITY_STANDALONE || UNITY_EDITOR 

        if (target) {

            float zoom = 0;
            
            if (Input.GetMouseButtonDown(0)) {
                if (EventSystem.current.IsPointerOverGameObject()) {
                    isOverUI = true;
                } 
            }
            if (Input.GetMouseButtonUp(0)) {
                isOverUI = false;
            }
            if (!isOverUI) { 
                if (!EventSystem.current.IsPointerOverGameObject()){
                    zoom = Input.GetAxis("Mouse ScrollWheel"); 
                }
                if (Input.GetMouseButton(0) || Input.touches.Length > 0) {
                    x += Input.GetAxis("Mouse X") * xSpeed * 1f * 0.02f;
                    y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;
                }
                    
                y = ClampAngle(y, Y_MIN, Y_MAX);

                var rotation = Quaternion.Euler(y, x, 0);

                distance = Mathf.Clamp(distance - zoom * 5, distanceMin, distanceMax);

                var negDistance = new Vector3(0.0f, 0.0f, -distance);
                Vector3 position = rotation * negDistance + target.position;

                transform.rotation = rotation;
                transform.position = position;
            }
            
        }

#elif UNITY_IOS || UNITY_ANDROID

        Touch touch01 = Input.GetTouch(0);

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began) {
            if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)) {

                isOverUI = true;
            }
        }
        if (Input.GetTouch(0).phase == TouchPhase.Ended) {
            isOverUI = false;
        }
        if (Input.touchCount == 1) {

            switch (touch01.phase) {
                case TouchPhase.Began:
                    startPos = touch01.deltaPosition;
                    break;
                case TouchPhase.Moved:
                    if (!isOverUI) {
                        x += (startPos.x - touch01.deltaPosition.x) * xSpeed * 0.002f;
                        y += (startPos.y - touch01.deltaPosition.y) * ySpeed * 0.002f;
                    }

                break;
            }
        }
        if (Input.GetMouseButtonDown(0)) {
            touchPosAtStart = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
        if (Input.touchCount == 2) {
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            float prevMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float currentMagnitude = (touchZero.position - touchOne.position).magnitude;

            float difference = currentMagnitude - prevMagnitude;

            zoom = difference * 0.0001f;

        } else if (Input.GetMouseButton(0)) {
            Vector3 direction = touchPosAtStart - Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Camera.main.transform.position += direction;
            zoom = 0;
        }

        Quaternion rotation = Quaternion.Euler(y, -x, 0);
        distance = Mathf.Clamp(distance - zoom * 5, distanceMin, distanceMax);
        Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
        Vector3 position = rotation * negDistance + target.position;

        transform.rotation = rotation;
        transform.position = position;
#endif

    }

    public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;
        return Mathf.Clamp(angle, min, max);
    }
}
