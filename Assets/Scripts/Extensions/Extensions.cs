﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public static class Extensions
{
    public static void SetCursor(Texture2D texture)
    {
        if (texture) {
            Cursor.SetCursor(texture, new Vector2(0.5f * texture.width, 0.5f * texture.height), CursorMode.Auto);
        } else {
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        }
    }

    

    public static Vector2 GetMousePosRelative(this RectTransform rectTransform, bool invertY = false)
    {
        Vector3[] corners = new Vector3[4];
        rectTransform.GetWorldCorners(corners);
        if (invertY)
            return new Vector2(Input.mousePosition.x - corners[0].x, corners[2].y - Input.mousePosition.y);
        else
            return new Vector2(Input.mousePosition.x - corners[0].x, Input.mousePosition.y - corners[0].y);
    }

    public static Vector2 GetWidthHeight(this RectTransform rectTransform)
    {
        Vector3[] corners = new Vector3[4];
        rectTransform.GetWorldCorners(corners);
        return new Vector2(corners[2].x - corners[0].x, corners[2].y - corners[0].y);
    }

    public static bool IsMouseDirectlyOver(this RectTransform rectTransform)
    {
        var eventDataCurrentPosition = new PointerEventData(EventSystem.current);
#if UNITY_STANDALONE || UNITY_EDITOR
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
#elif UNITY_IOS || UNITY_ANDROID
        eventDataCurrentPosition.position = new Vector2(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y);
#endif
        var results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        if  (results.Count > 0 && results[0].gameObject == rectTransform.gameObject) {
            return true;
        }
        return false;
    }

    public static T GetOrAddComponent<T>(this GameObject gameObject) where T : Component {
        T component = gameObject.GetComponent<T>();
        if (!component) {
            component = gameObject.AddComponent<T>();
        }
        return component;
    }

    public static bool IsDirectlyOverUI(Vector2 position)
    { 
        var eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = position;
        var results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        if (results.Count > 0) {
            return true;
        }
        return false;
    }

    public static float GetLineHeight(this UnityEngine.UI.Text text)
    {
        Vector2 extents = text.cachedTextGenerator.rectExtents.size * 0.5f;
        var lineHeight = text.cachedTextGeneratorForLayout.GetPreferredHeight("A", text.GetGenerationSettings(extents));

        return lineHeight;
    }

    public static Bounds? GetTreeBounds(this Transform transform)
    {
        bool first = true;
        var bounds = new Bounds();
               
        Renderer[] renderers = transform.GetComponentsInChildren<Renderer>();

        foreach (Renderer renderer in renderers)
        {
            bounds.Encapsulate(renderer.bounds);
        }

        //var tsms = new Stack<Transform>();
        //tsms.Push(transform);
        //
        //Renderer renderer;
        //
        //while (tsms.Count != 0) {
        //
        //    Transform current = tsms.Pop();
        //    renderer = current.GetComponent<Renderer>();
        //
        //    if (renderer != null) {
        //        if (first) {
        //            first = false;
        //            bounds = renderer.bounds;
        //        } else {
        //            bounds.Encapsulate(renderer.bounds);
        //        }
        //    }
        //
        //    foreach (Transform child in current) {
        //        tsms.Push(child);
        //    }
        //}

        if (renderers.Length == 0)
            return null;

        return bounds;
    }

    /// <summary>
    /// Returns an array of renderers from given GameObjects.
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public static Renderer[] GetRenderers(this IList<GameObject> input) {
        var renderers = new List<Renderer>();
        for (int i = 0; i < input.Count; i++) {
            Renderer renderer = input[i].GetComponent<Renderer>();
            if (!renderer)
                continue;
            renderers.Add(renderer);
        }
        return renderers.ToArray();
    }

    /// <summary>
    /// Returns bounds in world space
    /// </summary>
    /// <param name="gameObject">Object to take bounds from</param>
    /// <returns></returns>
    public static Bounds GetBoundsWorldSpace(this GameObject gameObject, bool includeChildren) {
        if (includeChildren) {
            List<GameObject> gameObjects = gameObject.GetChildren(true, true);
            return gameObjects.GetBoundsWorldSpace();
        } else {
            return new GameObject[] { gameObject }.GetBoundsWorldSpace();
        }
    }

    /// <summary>
    /// Returns bounds in world space
    /// </summary>
    /// <param name="gameObject">Objects to take bounds from</param>
    /// <returns></returns>
    public static Bounds GetBoundsWorldSpace(this IList<GameObject> gameObjects) {
        Renderer[] renderers = gameObjects.GetRenderers();
        if (renderers.Length == 0)
            throw new Exception("This requires at least one Renderer !");

        Bounds bounds = renderers[0].bounds;
        foreach (Renderer renderer in renderers) {
            // not sure if it really works with complex transformations...
            bounds.Encapsulate(renderer.bounds);
        }
        return bounds;
    }

    /// <summary>
    /// Returns all children (recursive) of given GameObjects
    /// </summary>
    /// <param name="gameObjects">Input GameObjects</param>
    /// <param name="recursive">If true, it will gather children recursively</param>
    /// <param name="includeParent">If true, input GameObjects will be present in the output</param>
    /// <returns></returns>
    public static IList<GameObject> GetChildren(this IList<GameObject> gameObjects, bool recursive, bool includeParent) {
        HashSet<GameObject> output = new HashSet<GameObject>();
        for (int i = 0; i < gameObjects.Count; i++) {
            foreach (GameObject gameOject in gameObjects[i].GetChildren(recursive, false)) {
                output.Add(gameOject);
            }
            if (includeParent)
                output.Add(gameObjects[i]);
        }
        return output.ToArray();
    }

    /// <summary>
    /// Returns all children (recursive) of given GameObjects
    /// </summary>
    /// <param name="gameObjects">Input GameObject</param>
    /// <param name="recursive">If true, it will gather children recursively</param>
    /// <param name="includeParent">If true, input GameObject will be present in the output</param>
    /// <returns></returns>
    public static List<GameObject> GetChildren(this GameObject gameObject, bool recursive, bool includeParent) {
        List<GameObject> output = new List<GameObject>();
        if (includeParent)
            output.Add(gameObject);
        if (gameObject.transform.childCount == 0)
            return output;
        Stack<GameObject> stack = new Stack<GameObject>();
        stack.Push(gameObject);
        while (stack.Count != 0) {
            GameObject current = stack.Pop();
            foreach (Transform child in current.transform) {
                stack.Push(child.gameObject);
                output.Add(child.gameObject);
            }
            if (!recursive)
                break;
        }
        return output;
    }

    public static void AddMaterial(this Renderer renderer, Material materialToAdd) {
        Material[] originalMaterials = renderer.sharedMaterials;
        Material[] materials = new Material[originalMaterials.Length + 1];
        for (int i = 0; i < materials.Length - 1; i++) {
            if (originalMaterials[i] == materialToAdd)
                return;
            materials[i] = originalMaterials[i];
        }
        materials[materials.Length - 1] = materialToAdd;
        renderer.sharedMaterials = materials;
    }

    public static void RemoveMaterial(this Renderer renderer, Material materialToRemove) {
        Material[] originalMaterials = renderer.sharedMaterials;
        var materials = new List<Material>(renderer.sharedMaterials.Length);
        for (int i = 0; i < originalMaterials.Length; i++) {
            if (originalMaterials[i] != materialToRemove) {
                materials.Add(originalMaterials[i]);
            }
        }
        if (materials.Count != originalMaterials.Length)
            renderer.sharedMaterials = materials.ToArray();
    }
}
