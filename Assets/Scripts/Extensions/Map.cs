﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Map<T1, T2> : IEnumerable<Tuple<T1, T2>>
{

    private Dictionary<T1, T2> _dic1 = new Dictionary<T1, T2>();
    private Dictionary<T2, T1> _dic2 = new Dictionary<T2, T1>();

    public void Add(T1 t1, T2 t2)
    {
        _dic1.Add(t1, t2);
        _dic2.Add(t2, t1);
    }

    public void AddOrSet(T1 t1, T2 t2)
    {
        if (_dic1.ContainsKey(t1)) {
            _dic1[t1] = t2;
        } else {
            _dic1.Add(t1, t2);
        }
        if (_dic2.ContainsKey(t2)) {
            _dic2[t2] = t1;
        } else {
            _dic2.Add(t2, t1);
        }
    }

    public bool Contains1(T1 t1)
    {
        return _dic1.ContainsKey(t1);
    }

    public bool Contains2(T2 t2)
    {
        return _dic2.ContainsKey(t2);
    }

    public IEnumerator<Tuple<T1, T2>> GetEnumerator()
    {
        throw new Exception();
    }

    public void Remove1(T1 t1)
    {
        _dic2.Remove(_dic1[t1]);
        _dic1.Remove(t1);
    }

    public void Remove2(T2 t2)
    {
        _dic1.Remove(_dic2[t2]);
        _dic2.Remove(t2);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return _dic1.GetEnumerator();
    }

    public T1[] GetValues1()
    {
        return _dic1.Keys.ToArray();
    }

    public T2[] GetValues2()
    {
        return _dic2.Keys.ToArray();
    }

    public void Clear()
    {
        _dic1.Clear();
        _dic2.Clear();
    }

    public T2 Get1(T1 t1)
    {
        return _dic1[t1];
    }

    public T1 Get2(T2 t2)
    {
        return _dic2[t2];
    }
}