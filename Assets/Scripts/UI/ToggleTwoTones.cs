﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Toggle))]
public class ToggleTwoTones : MonoBehaviour
{
    private Toggle toggle;
    public GameObject panel;

    void Start()
    {
        toggle = GetComponent<Toggle>();
        toggle.onValueChanged.AddListener(OnToggleValueChanged);
        OnToggleValueChanged(toggle.isOn);
        ColorBlock cb = toggle.colors;
    }

    private void OnToggleValueChanged(bool isOn)
    {
        ColorBlock cb = toggle.colors;
        if (!isOn) {
            cb.normalColor = Color.gray;
            cb.pressedColor = Color.gray;
            cb.highlightedColor = Color.gray;
            cb.selectedColor = Color.gray;
        } else {
            cb.normalColor = Color.white;
            cb.pressedColor = Color.white;
            cb.highlightedColor = Color.white;
            cb.selectedColor = Color.white;
        }
        toggle.colors = cb;
    }

    private void Update()
    {
        if(panel != null)
        toggle.isOn = panel.activeSelf;
    }
    
}
