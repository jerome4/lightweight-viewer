﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class UIWindow : MonoBehaviour
{
    public EventHandler onValidated;
    public EventHandler onCanceled;
    private UIBuilder uiManager;

    void Awake()
    {
#if UNITY_STANDALONE || UNITY_EDITOR 
        transform.SetParent(FindObjectOfType<CanvasScaler>().transform, false);
#endif
        transform.localPosition = new Vector3(0, 0, 0);
        uiManager = GetComponentInParent<UIBuilder>();
        //GetComponent<Image>().color = new Color(ThemeRefs.Color2.r, ThemeRefs.Color2.g, ThemeRefs.Color2.b, 0.9f);
        GetComponent<Image>().color = ThemeRefs.Color2;
    }

    public void cancel()
    {
        Destroy(this.gameObject);
    }
}
