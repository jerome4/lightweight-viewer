﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ToggleLabelControl : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI propertyName;

    [SerializeField]
    private TextMeshProUGUI valueName;

    [SerializeField]
    private Toggle toggle;

    public string Property {
        get {
            return propertyName.text;
        }
        set {
            propertyName.text = value;
        }
    }

    public string Value
    {
        get {
            return valueName.text;
        }
        set {
            valueName.text = value;
        }
    }

    public Toggle Toggle => toggle;
}
