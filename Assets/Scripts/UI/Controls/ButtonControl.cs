﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ButtonControl : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI labelName;

    [SerializeField]
    private Button button;

    public string Name {
        get {
            return labelName.text;
        }
        set {
            labelName.text = value;
        }
    }

    public Button Button => button;

    private void Awake()
    {
        button.image.color = ThemeRefs.Color3;
        labelName.color = ThemeRefs.Text1;
    }
}
