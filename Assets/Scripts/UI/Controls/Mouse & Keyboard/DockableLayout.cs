﻿using UnityEngine;
using DynamicPanels;
using System;
using System.Collections.Generic;

[RequireComponent(typeof(DynamicPanelsCanvas))]
public class DockableLayout : Layout
{
    public Container containerPrefab;

    private DynamicPanelsCanvas dynamicCanvas;

    private Dictionary<(Direction, Direction), DynamicPanelsCanvas.AnchoredPanelProperties> panels = new Dictionary<(Direction, Direction), DynamicPanelsCanvas.AnchoredPanelProperties>();

    public DynamicPanelsCanvas.AnchoredPanelProperties GetPanel(Direction dir1, Direction dir2) {
        if (panels.ContainsKey((dir1, dir2)))
            return panels[(dir1, dir2)];

        var anchoredPanel = new DynamicPanelsCanvas.AnchoredPanelProperties();
        if (dir1 != Direction.None) {
            if (dir2 != Direction.None) {
                var parentPanel = GetPanel(dir1, Direction.None);
                anchoredPanel.anchorDirection = dir2;
                parentPanel.subPanels.Add(anchoredPanel);
            } else {
                anchoredPanel.anchorDirection = dir1;
                dynamicCanvas.Internal.InitialPanelsAnchored.subPanels.Add(anchoredPanel);
            }
        } else {
            dynamicCanvas.Internal.InitialPanelsUnanchored.Add(anchoredPanel.panel);
        }
        panels.Add((dir1, dir2), anchoredPanel);
        return anchoredPanel;
    }

    void Start()
    {
        dynamicCanvas = GetComponent<DynamicPanelsCanvas>();
        dynamicCanvas.DoAwake();

        foreach (Type type in BasePanel.PanelTypes) {
            var panel = BasePanel.Create(type, transform, containerPrefab);

            var panelTabProperties = new DynamicPanelsCanvas.PanelTabProperties {
                content = panel.GetComponent<RectTransform>(),
                id = panel.PanelName,
                tabLabel = panel.PanelName,
                tabIcon = panel.PanelIcon,
                minimumSize = new Vector2(270, 150)
            };

            switch (panel.PanelName.ToLower()) {
                case "hierarchy":
                    GetPanel(Direction.Top, Direction.Left).panel.tabs.Add(panelTabProperties);
                    break;
                case "interaction":
                    GetPanel(Direction.Right, Direction.Bottom).panel.tabs.Add(panelTabProperties);
                    break;
                case "import":
                    GetPanel(Direction.Top, Direction.Left).panel.tabs.Add(panelTabProperties);
                    break;
                case "metadata":
                    GetPanel(Direction.Right, Direction.Top).panel.tabs.Add(panelTabProperties);
                    break;
                case "visualization":
                    GetPanel(Direction.Right, Direction.None).panel.tabs.Add(panelTabProperties);
                    break;
                default:
                    GetPanel(Direction.Right, Direction.None).panel.tabs.Add(panelTabProperties);
                    break;
            }
        }
        dynamicCanvas.DoStart();
    }
}