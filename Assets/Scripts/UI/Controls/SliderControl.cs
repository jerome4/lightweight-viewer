﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SliderControl : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI labelName;

    [SerializeField]
    private Slider slider;

    public string Name {
        get {
            return labelName.text;
        }
        set {
            labelName.text = value;
        }
    }

    public Slider Slider => slider;
}
