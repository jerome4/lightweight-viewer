﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DropdownControl : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI labelName;

    [SerializeField]
    private TMP_Dropdown dropdown;

    [SerializeField]
    private Image backgroundImage;

    [SerializeField]
    private Image dropdownImage;

    public string Name {
        get {
            return labelName.text;
        }
        set {
            labelName.text = value;
        }
    }

    public TMP_Dropdown Dropdown => dropdown;

    private void Awake()
    {
        backgroundImage.color = ThemeRefs.Color3;
        dropdown.captionText.color = ThemeRefs.Text1;

        dropdownImage.color = ThemeRefs.Color3;
        dropdown.itemText.color = ThemeRefs.Text1;
    }
}