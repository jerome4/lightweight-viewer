﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;


public class TouchscreenLandscapeLayout : Layout {
    public Container containerPrefab;
    public GameObject androidMenu;

    private const float PANELSIZE_X = 600;
    private const float MENUSIZE_X = 200;
    private List<BasePanel> panels = new List<BasePanel>();
    private List<Button> buttons = new List<Button>();

    void Start()
    {
        //_androidMenu = Instantiate(androidMenu,transform);
        androidMenu.GetComponent<RectTransform>().sizeDelta = new Vector2(MENUSIZE_X, 1);
        androidMenu.GetComponent<RectTransform>().position = new Vector2(MENUSIZE_X / 2,
            GetComponent<RectTransform>().GetWidthHeight().y / 2);

        foreach (Type type in BasePanel.PanelTypes) {
            var panel = BasePanel.Create(type, transform, containerPrefab);
            RectTransform rectTransform = panel.GetComponent<RectTransform>();
            panel.GetComponent<RectTransform>().sizeDelta = new Vector2(PANELSIZE_X, 0);
            panel.GetComponent<RectTransform>().position = new Vector2(
                GetComponent<RectTransform>().GetWidthHeight().x - (PANELSIZE_X / 2),
                GetComponent<RectTransform>().GetWidthHeight().y / 2);

            var button = new GameObject();
            button.transform.SetParent(androidMenu.transform.GetChild(0));
            Image imageComponent = button.AddComponent<Image>();
            imageComponent.sprite = panel.PanelIcon;
            Button ButtonComponent = button.AddComponent<Button>();
            ButtonComponent.name = panel.PanelName + "_Toggle";
            ButtonComponent.onClick.AddListener(delegate { SwitchPanel(panel); });
            buttons.Add(ButtonComponent);

            panels.Add(panel);
            panel.gameObject.SetActive(false);
        }
    }

    ColorBlock SetColorBlock(ColorBlock colorBlock, bool isOn)
    {
        if (isOn) {
            colorBlock.selectedColor = ThemeRefs.Accent;
            colorBlock.normalColor = ThemeRefs.Accent;
            colorBlock.pressedColor = ThemeRefs.Accent;
            colorBlock.highlightedColor = ThemeRefs.Accent;
        } else {
            colorBlock.selectedColor = Color.white;
            colorBlock.normalColor = Color.white;
            colorBlock.pressedColor = Color.white;
            colorBlock.highlightedColor = Color.white;
        }
        return colorBlock;
    }

    private void SwitchPanel(BasePanel panel)
    {
        for (int i = 0; i < panels.Count; i++) {
            if (panels[i] != panel) {
                panels[i].gameObject.SetActive(false);
            } else {
                if (panels[i].gameObject.activeSelf == false) {
                    panels[i].gameObject.SetActive(true);
                } else {
                    panels[i].gameObject.SetActive(false);

                }
                //panels[i].gameObject.SetActive(panels[i].gameObject.activeSelf);
            }
            ColorBlock colorBlock = buttons[i].colors;
            buttons[i].colors = SetColorBlock(colorBlock, panels[i].gameObject.activeSelf);
        }
    }
}

