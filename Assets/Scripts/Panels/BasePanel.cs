﻿using System;
using System.Reflection;
using UnityEngine;
using System.Linq;
using System.Collections.Generic;


[RequireComponent(typeof(Container))]
public abstract class BasePanel : MonoBehaviour {
    #region Static

    public static T Create<T>(Transform parent, Container containerPrefab) where T : BasePanel
    {
        return (T)Create(typeof(T), parent, containerPrefab);
    }

    public static BasePanel Create(Type type, Transform parent, Container containerPrefab)
    {
        Container container = Instantiate(containerPrefab);
        var panel = (BasePanel)container.gameObject.AddComponent(type);
        panel.container = container;
        container.name = panel.PanelName;
        container.transform.SetParent(parent);
        container.transform.localScale = Vector3.one;
        return panel;
    }

    private static Type[] panelTypes;
    public static Type[] PanelTypes
    {
        get {
            if (panelTypes == null) {
                panelTypes = Assembly.GetAssembly(typeof(BasePanel))
                    .GetTypes()
                    .Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(BasePanel)))
                    .ToArray();
            }
            return panelTypes;
        }
    }

    #endregion

    private Container container;
    public Container Container => container ?? (container = GetComponent<Container>());

    private void Awake()
    {
        InteractionModel.changed.AddListener(() => { ClearControls(); RefreshGUI(); });
    }

    public GameObject AddControl(GameObject prefab, bool addOnTop = false)
    {
        GameObject newControl = Instantiate(prefab);
        newControl.transform.SetParent(Container.Content);
        newControl.transform.localScale = Vector3.one;
        if (addOnTop)
            newControl.transform.SetSiblingIndex(0);
        return newControl;
    }

    public T AddControl<T>(T prefab, bool addOnTop = false) where T : Component
    {
        T newControl = Instantiate(prefab);
        newControl.transform.SetParent(Container.Content);
        newControl.transform.localScale = Vector3.one;
        if (addOnTop)
            newControl.transform.SetSiblingIndex(0);
        return newControl;
    }

    public void ClearControls()
    {
        List<GameObject> children = gameObject.GetChildren(true, false);
        foreach (GameObject child in children) {
            DestroyImmediate(child.gameObject);
        }
    }

    public abstract void RefreshGUI();

    public abstract string PanelName { get; }

    public abstract Sprite PanelIcon { get; }
}
