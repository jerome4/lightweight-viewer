﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Pixyz.Import;

public class MetadataPanel : BasePanel
{
    public override string PanelName => "Metadata";
    public override Sprite PanelIcon => Resources.Load<Sprite>("Icons/Group");

    private MetadataControl metadataControl;
    public List<MetadataControl> metadataObjects = new List<MetadataControl>();

    private void Awake()
    {
        metadataControl = InteractionModel.Current.metadataControl;
        Selection.changed.AddListener(selectionChanged);
        RefreshGUI();
    }

    override public void RefreshGUI()
    {

    }

    private void selectionChanged()
    {
        cleanMetadata();
        var selection = Selection.Selected;
        if (selection == null || selection.GetComponent<Metadata>() == null) return;
        foreach (var property in selection.GetComponent<Metadata>().getProperties()) {
            var control = AddControl(metadataControl);
            control.property.text = property.Key;
            control.value.text = property.Value;
            metadataObjects.Add(control);
        }
    }

    private void cleanMetadata()
    {
        foreach(var meta in metadataObjects) {
            DestroyImmediate(meta.gameObject, true);
        }
        metadataObjects.Clear();
    }    
}
