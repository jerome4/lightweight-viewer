﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class HierarchyPanel : BasePanel
{
    public override string PanelName => "Hierarchy";
    public override Sprite PanelIcon => Resources.Load<Sprite>("Icons/Hierarchy");

    private HierarchyControl hierarchyControl;

    private void Awake()
    {
        hierarchyControl = InteractionModel.Current.hierarchyControl;
        RefreshGUI();
    }

    override public void RefreshGUI()
    {
        AddControl(hierarchyControl);
    }



    /*


    abstract public void AddNode(Transform transform);*/
}
