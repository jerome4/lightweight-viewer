﻿using UnityEngine;

public class ImageRefs : ProjectReferences<ImageRefs>
{
    [SerializeField]
    private Sprite uncollapseButtonIcon;
    public static Sprite UncollapseButtonIcon => Instance.uncollapseButtonIcon;

    [SerializeField]
    private Sprite collapseButtonIcon;
    public static Sprite CollapseButtonIcon => Instance.collapseButtonIcon;

    [SerializeField]
    private Texture uvTexture;
    public static Texture UvTexture => Instance.uvTexture;
}