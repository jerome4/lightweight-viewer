﻿Shader "PiXYZ/Wireframe Overlay"
{
   Properties
   {
      _Gain("Gain", Float) = 1.5
      _Color("Color", Color) = (1,1,1,1)
      _EdgeColor("Edge Color", Color) = (0,0,0,1)
      [Toggle] _RemoveDiag("Remove diagonals", Float) = 0.0
      _Transparency("Transparency", Range(0.0, 1.0)) = 0.0
		[Toggle()] _clipPlaneEnabled("Cliplane enable", Float) = 0.0
   }

   SubShader
   {
      Tags {"Queue" = "Transparent" "RenderType" = "Opaque" }
      Blend SrcAlpha OneMinusSrcAlpha
      LOD 100

      Pass
      {
         CGPROGRAM

         #pragma target 3.0
         #pragma glsl
         #pragma vertex vert
         #pragma geometry geom
         #pragma fragment frag
         #pragma multi_compile __ _REMOVEDIAG_ON
         #include "UnityCG.cginc"

         struct appdata
         {
            float4 vertex : POSITION;
         };

         struct v2g
         {
            float4 vertex : SV_POSITION;
			int clipPlaneEnabled : FLOAT;
         };

         struct g2f
         {
            float4 vertex : SV_POSITION;
            float3 bary : TEXCOORD1;
			float3 worldSpacePos : FLOAT3;
			int clipPlaneEnabled : FLOAT;
         };

         float _Gain;
         float4 _Color;
         float4 _EdgeColor;
		 float4 _Plane;
		 float _clipPlaneEnabled;

         v2g vert(appdata v)
         {
            v2g o;
            o.vertex = mul(unity_ObjectToWorld, v.vertex);
			o.clipPlaneEnabled = (int)_clipPlaneEnabled;
            return o;
         }

         [maxvertexcount(3)]
         void geom(triangle v2g IN[3], inout TriangleStream<g2f> triStream) {
            float3 param = float3(0.0, 0.0, 0.0);

            #if _REMOVEDIAG_ON
            float EdgeA = length(IN[0].vertex - IN[1].vertex);
            float EdgeB = length(IN[1].vertex - IN[2].vertex);
            float EdgeC = length(IN[2].vertex - IN[0].vertex);

            if (EdgeA > EdgeB && EdgeA > EdgeC)
               param.y = 1.0;
            else if (EdgeB > EdgeC && EdgeB > EdgeA)
               param.x = 1.0;
            else
               param.z = 1.0;
            #endif

            g2f o;
            o.vertex = mul(UNITY_MATRIX_VP, IN[0].vertex);
            o.bary = float3(1.0, 0.0, 0.0) + param;
			o.worldSpacePos = IN[0].vertex;
			o.clipPlaneEnabled = IN[0].clipPlaneEnabled;
            triStream.Append(o);

            o.vertex = mul(UNITY_MATRIX_VP, IN[1].vertex);
            o.bary = float3(0.0, 0.0, 1.0) + param;
			o.worldSpacePos = IN[1].vertex;
			o.clipPlaneEnabled = IN[1].clipPlaneEnabled;
            triStream.Append(o);

            o.vertex = mul(UNITY_MATRIX_VP, IN[2].vertex);
            o.bary = float3(0.0, 1.0, 0.0) + param;
			o.worldSpacePos = IN[1].vertex;
			o.clipPlaneEnabled = IN[1].clipPlaneEnabled;
            triStream.Append(o);
         }

         float edgeFactor(float3 bary)
         {
            float3 d = fwidth(bary);
            float3 a3 = smoothstep(float3(0, 0, 0), _Gain * d, bary);
            return min(min(a3.x, a3.y), a3.z);
         }

         float _Transparency;

         fixed4 frag(g2f i) : SV_Target
         {
            fixed4 col = 0;
            float t = edgeFactor(i.bary);
            col = lerp(_EdgeColor, col, t);
            col.a *= _Transparency;

			if (i.clipPlaneEnabled == 1.0f)
			{
				float distance = dot(i.worldSpacePos, _Plane.xyz);
				distance = distance + _Plane.w;
				//discard surface above plane
				clip(-distance);
			}

            return col;
         }
         ENDCG
      }
   }
}