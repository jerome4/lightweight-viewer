﻿Shader "PiXYZ/Vertex Color Overlay"
{
	Properties
	{
		_Transparency("Transparency", Range(0.0, 1.0)) = 0
		[Toggle()] _clipPlaneEnabled("Cliplane enable", Float) = 0.0
	}

	SubShader
	{
		Tags {"Queue" = "Transparent" "RenderType" = "Opaque" }
		Blend SrcAlpha OneMinusSrcAlpha
		LOD 100

		Pass
		{
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile _ UNITY_SINGLE_PASS_STEREO STEREO_INSTANCING_ON STEREO_MULTIVIEW_ON
			#include "UnityCG.cginc"

			float4 _Plane;
			float _clipPlaneEnabled;

			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f
			{
				float4 color : COLOR;
				float4 pos : SV_POSITION;
				float3 worldSpacePos : FLOAT3;
				int clipPlaneEnabled : FLOAT;
				UNITY_VERTEX_OUTPUT_STEREO
			};

			v2f vert(appdata v)
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				o.pos = UnityObjectToClipPos(v.vertex);
				o.color = v.color;
				o.worldSpacePos = mul(unity_ObjectToWorld, v.vertex);
				o.clipPlaneEnabled = (int)_clipPlaneEnabled;

				return o;
			}

			float _Transparency;

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 color = i.color;
				color.a = _Transparency;

				if (i.clipPlaneEnabled == 1.0f)
				{
					float distance = dot(i.worldSpacePos, _Plane.xyz);
					distance = distance + _Plane.w;
					//discard surface above plane
					clip(-distance);
				}

				return color;
			}

			ENDCG
		}
	}
}